$(document).ready(function() {

    var $room_code = $('input[name=room_code]').val();
    var $username = $('input[name=username]').val();

    var socket = io();
    socket.emit('joinRoom', $room_code);

    var vue = new Vue({
        el: '#chatPanel',

        data: {
            messages: [],
            message: '',
        },

        ready: function() {

            socket.on('broadcastNewMessage', function(username, message, uuid) {

                this.messages.push({
                    uuid: uuid,
                    message: username + ': '+ message,
                    numOfLikes: '',
                    liked: false
                });

            }.bind(this));

            socket.on('broadcastNewLike', function(username, uuid) {

                var messageIndex = this.messages.map(function(x) { return x.uuid; }).indexOf(uuid.toString());

                if (messageIndex == -1) {

                    return;

                }

                var likeCount = 0;

                // if like is not empty, set to the existing like count
                if (this.messages[messageIndex].numOfLikes != '') {

                    likeCount = parseInt(this.messages[messageIndex].numOfLikes.substring(1), 10);
                }

                likeCount++;

                this.messages[messageIndex].numOfLikes = '+' + likeCount;


            }.bind(this));


        },

        methods: {
            send: function(e) {
                socket.emit('submitMessage', $room_code, $username, this.message);

                this.message = '';

                e.preventDefault();
            },

            like: function(e) {

                var uuid = $(e.currentTarget).data('id');

                var likeButton = $(e.target).closest('button');

                // toggle the like button
                likeButton.removeClass('btn-primary');
                likeButton.addClass('btn-success');
                likeButton.attr('disabled', 'true');

                if (uuid == null) {
                    alert("Something went wrong. Please refresh and try again.");
                    return;
                }

                socket.emit('likeMessage', $room_code, $username, uuid);

            }
        }
    });

    // on first load: fill in chat box with cached message
    renderCachedMessages();


    function renderCachedMessages() {

        if ($('div#cached_message').text() != '') {

            var cached_message = JSON.parse($('div#cached_message').text());


            for (var i = 0; i < cached_message.length; i++) {


                // fill in likes if there are any
                var numOfLikes = '';
                if (cached_message[i].listOfLikes.length != 0) {

                    numOfLikes = '+' + cached_message[i].listOfLikes.length;

                }

                // set flag for the like button status
                var liked = false;
                if (cached_message[i].listOfLikes.includes($username)) {
                    liked = true;
                }

                // load into vue component
                vue.messages.push({
                    uuid: cached_message[i].uuid,
                    message: cached_message[i].username + ": " + cached_message[i].message,
                    numOfLikes: numOfLikes,
                    liked: liked
                });

            }

        }
    }

});