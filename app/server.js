/* Modules */
var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var bodyParser = require('body-parser');
var path = require('path');
var stringify = require('json-stringify');


/* Global Variables */
global.messageCache = {};


/* Express Setup */
app.use(express.static(__dirname + '/public'));
app.set('views', path.join(__dirname, '/public/views'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');


/* Route: GET */
app.get('/', function(request, response) {
    response.render('index');
});
app.get('/student', function(request, response) {
    response.render('student');
});
app.get('/teacher', function(request, response) {
    response.render('teacher');
});
app.get('/room', function(request, response) {// from student login
    var username = request.query.username;
    var room_code = request.query.room_code;

    if (!io.sockets.adapter.rooms.hasOwnProperty(room_code)) {
        response.render('student', { error_message: 'Room does not exist.', username: username, room_code: room_code });
        return;
    }

    response.render('room', {
        username: username,
        room_code: room_code,
        cached_message: JSON.stringify(global.messageCache[room_code])
    });

});


/* Route: POST */
app.post('/room', function(request, response) { // from teacher login

    var username = request.body.username;
    var room_code = request.body.room_code;

    response.render('room', {
        username: username,
        room_code: room_code,
        cached_message: JSON.stringify(global.messageCache[room_code])
    });

});


/* Start Server */
server.listen(8080);


/* Socket IO setup */
io.on('connection', function(socket) {

    socket.on('joinRoom', function(room_code) {
        socket.join(room_code);
    });

//    socket.on('disconnect', function() {
//        io.emit('chat.message', 'User has disconnected.');
//    });

    socket.on('submitMessage', function (room_code, username, message) {

        // generate uuid for the message
        var uuid = Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)

        // if first message (no storage made yet)
        if (!global.messageCache.hasOwnProperty(room_code)) {
            global.messageCache[room_code] = [];
        }

        // if cache is full, cut it
        while (global.messageCache[room_code].length >= 100) {
            global.messageCache[room_code].splice(0, 1);
        }

        // cache the message
        global.messageCache[room_code].push({
            "uuid": uuid,
            "username": username,
            "message" : message,
            "listOfLikes" : []
        });

        // broadcast the new message
        io.sockets.in(room_code).emit('broadcastNewMessage', username, message, uuid);

    });

    socket.on('likeMessage', function (room_code, username, uuid) {

        if (uuid == null) {
            return;
        }

        var globalMessageCacheIndex = global.messageCache[room_code].map(function(x) {return x.uuid; }).indexOf(uuid.toString());

        // if message exists in cache
        if (globalMessageCacheIndex != '-1') {

            // if username has not like
            if (!global.messageCache[room_code][globalMessageCacheIndex].listOfLikes.includes(username)) {

                // update cache message
                global.messageCache[room_code][globalMessageCacheIndex].listOfLikes.push(username);

            }

        }

        // broadcast the like
        io.sockets.in(room_code).emit('broadcastNewLike', username, uuid);

    });


});
